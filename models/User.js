const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Username required."],
		unique: true
	},
	email: {
		type: String,
		required: [true, "Email required."],
		unique: true
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	password: {
		type: String,
		required: [true, "Password required."]
	}
},{timestamps: true})

module.exports = mongoose.model('User', UserSchema)