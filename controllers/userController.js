const User = require('./../models/User')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const {createAccessToken} = require("../utils/auth.js")
require('dotenv/config')
const {sendRegisterMail,sendRegistryMail} = require('../utils/sendMail')

module.exports.register = async (req,res) => {
	const hashedPw = await bcrypt.hashSync(req.body.password, 10)
	if(req.body.password.length < 8) return res.send({message: "Password is too short."})
	if (req.body.password !== req.body.confirmPassword) return res.send({message:"Passwords don't match."})

	let newUser = new User({
			username: req.body.username.toLowerCase(),
			email: req.body.email.toLowerCase(),
			password: hashedPw
	})
	await newUser.save()
	.then(user=>{
		sendRegistryMail(req.body)
		sendRegisterMail(req.body)
		res.send({user,message: "Registration Successful!"})
	})
	.catch(error=>{res.send(error)})
}

module.exports.login = async (req,res) => {
	let {user,password} = req.body
	await User.findOne({$or:[{username:user.toLowerCase()},{email:user.toLowerCase()}]})
	.then(foundUser=>{
		if(foundUser === null){
			res.send({message: 'Invalid credentials.'})
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			if(isPasswordCorrect){
				res.send({accessToken: createAccessToken(foundUser), message: "Login Successful!"})
			} else {
				res.send({message: "Invalid credentials."})
			}
		}
	})
	.catch(err=>console.log(err))
}


module.exports.deleteOneUser = async (req,res) => {
	await User.findByIdAndDelete(req.params.id)
	.then((deletedUser) => {
		res.send({message:`User has been deleted.`})
	})
	.catch(error => res.send(error))
}

module.exports.getOneUser = async (req,res) => {
	User.findById(req.user.id,{password:0, _id:0, createdAt:0, updatedAt:0, __v:0})
	.then(foundUser => {
		res.send(foundUser)
	})
	.catch(error => res.send(error))
}


module.exports.getAllUsers = async (req,res)=> {
	await User.find({isAdmin: false},{password:0, isAdmin:0, createdAt:0, updatedAt:0, __v:0})
	.then(users => {
		if(users.length>0){
			return res.send({usersCount:`${users.length}`,users})
		} else {
			res.send({usersCount:`${users.length}`,message: "No user/s found."})
		}
	})
	.catch(error => res.send(error))
}


module.exports.deleteAllusers = async (req,res)=> {
	await User.deleteMany({isAdmin:false})
	.then(deletedUsers=>{
		res.send({deletedUsers, message: "Deleted all users!"})
	})
	.catch(error => res.send(error))
}


