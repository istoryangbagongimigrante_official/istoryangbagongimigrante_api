const express = require('express')
const app = express()
require('dotenv/config')
const port = process.env.PORT||4000
const cors = require('cors')

require('./db')

app.use(cors())

app.use(express.json())

app.use('/api/users', require('./routes/user'))

app.listen(port, ()=> console.log(`running on port ${port}`))