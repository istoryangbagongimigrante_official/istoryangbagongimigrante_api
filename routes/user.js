const router = require('express').Router()
const validate = require('../utils/validation')
const auth = require('../utils/auth')
const user = require('../controllers/userController')

router.post('/register',validate.userExists, user.register)

router.post('/login', user.login) 

router.get('/getAll', auth.verifyUser, auth.verfiyAdmin, user.getAllUsers)

router.get('/getUser', auth.verifyUser, user.getOneUser)

router.delete('/deleteAll', auth.verifyUser, auth.verfiyAdmin, user.deleteAllusers)

router.delete('/:id', auth.verifyUser, auth.verfiyAdmin, user.deleteOneUser)

module.exports = router