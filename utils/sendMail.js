const User = require('./../models/User')
const nodemailer = require('nodemailer')
const {google} = require('googleapis')
const CLIENT_ID = process.env.CLIENT_ID
const CLIENT_SECRET = process.env.CLIENT_SECRET
const REDIRECT_URI = process.env.REDIRECT_URI
const REFRESH_TOKEN = process.env.REFRESH_TOKEN
const EMAIL = process.env.EMAIL
const {capitalizeFirstChar} = require('./utilFunctions')

const oAuth2Client = new google.auth.OAuth2(CLIENT_ID,CLIENT_SECRET,REDIRECT_URI)
oAuth2Client.setCredentials({refresh_token: REFRESH_TOKEN})

module.exports.sendRegisterMail = async (user) =>{
    let name = capitalizeFirstChar(`${user.username}`)
    const mailAccessToken = await oAuth2Client.getAccessToken()
    const transport = nodemailer.createTransport({
        service: process.env.SERVICE,
        auth: {
            type: 'OAuth2',
            user: EMAIL,
            clientId: CLIENT_ID,
            clientSecret: CLIENT_SECRET,
            refreshToken: REFRESH_TOKEN,
            accessToken: mailAccessToken
        }
    })

    const mailOptions = {
        from: 'istoryangbagongimigrante <EMAIL>',
        to: user.email.toLowerCase(),
        subject: 'Registration Successful',
        text: `
        Hello ${name}!

        Your username is ${user.username.toLowerCase()}.
        The email you used is ${user.email.toLowerCase()}.

        Thank you for registering at istoryangbagongimigrante!
        Have fun and enjoy exploring the site.

        Sincerely,
        istoryangbagongimigrante`
    }
    await transport.sendMail(mailOptions)
    .then(data => console.log(data))
    .catch(error => console.loh(error))
}

module.exports.sendRegistryMail = async (user) =>{
    let name = capitalizeFirstChar(`${user.username}`)
    const mailAccessToken = await oAuth2Client.getAccessToken()
    const transport = nodemailer.createTransport({
        service: process.env.SERVICE,
        auth: {
            type: 'OAuth2',
            user: EMAIL,
            clientId: CLIENT_ID,
            clientSecret: CLIENT_SECRET,
            refreshToken: REFRESH_TOKEN,
            accessToken: mailAccessToken
        }
    })
    const mailOptions = {
        from: 'istoryangbagongimigrante>',
        to: EMAIL,
        subject: 'New user from istoryangbagongimigrante',
        text: `
        A new user has just signed up!

        Username: ${user.username.toLowerCase()}
        Email: ${user.email.toLowerCase()}

        Thank you note sent!
        `
    }
    await transport.sendMail(mailOptions)
    .then(data => console.log(data))
    .catch(error => console.loh(error))
}