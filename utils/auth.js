const jwt = require("jsonwebtoken")
const secret = process.env.SECRET

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
        username: user.username
    }
    return jwt.sign(data,secret,{})

}

module.exports.verifyUser = (req,res,next) => {
    let token = req.headers.authorization
    if(typeof token === "undefined"){
        res.send({auth: "You are not a registered user."})
    } else {
        token = token.slice(7,token.length)
        jwt.verify(token,secret, function(err,decoded){
            if(err){
                res.send({auth:"You are not a registered user."})
            } else {
                req.user = decoded
                next()
            }
        })
    }
}

module.exports.verfiyAdmin = (req,res,next) => {
    if(req.user.isAdmin){
        next()
    } else {
        res.send({auth: "You are not an admin."})
    }
}


