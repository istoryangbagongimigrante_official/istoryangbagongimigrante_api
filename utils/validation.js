const User = require('../models/User')
const bcrypt = require('bcrypt')

module.exports.userExists = (req,res,next) => {
	User.findOne({
		$or:[{username: req.body.username.toLowerCase()},{email: req.body.email.toLowerCase()}]
	})
	.then(foundResult => {
		if(foundResult){
			if(foundResult.username===req.body.username.toLowerCase()){
				res.send({message: "Username already taken."})
			} else if(foundResult.email===req.body.email.toLowerCase()){
				res.send({message: "Email already registered."})
			}
		} else {
			next()
		}
	})
	.catch(error => res.send(error))
	// console.log(req.body)
}

module.exports.checkPassword = async (req,res,next) => {
	await User.findById(req.user.id)
	.then(foundUser => {
		if(foundUser){
			if(foundUser._id.toString()===req.user.id){
				const isPasswordCorrect = bcrypt.compareSync(req.body.currentPassword, foundUser.password)
				if(isPasswordCorrect){
					if(req.body.newPassword===req.body.confirmNewPassword){
						next()
					} else {
						res.send({message: "New passwords does not match."})
					}
				} else {
					res.send({message: "Invalid credentials."})
				}
			} else {
				res.send({message: "You are not authorized to change the password of this user."})
			}
		} else {
			res.send({message: "User does not exist."})
		}
	})
	.catch(error => res.send(error))
}

